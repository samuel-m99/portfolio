import React from "react";

export default function Portfolio (props) {
    return (
        <div className={`card-${props.mode}`}>
            <h2>{props.name}</h2>
            {props.description && <p>{props.description}</p>}
            {props.url && <a href={props.url} target="_blank" rel="noreferrer">source code</a>}
        </div>
    );
}
